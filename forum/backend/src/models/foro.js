const mongoose = require('mongoose')

const foroSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
    min: 2,
    max: 40
  },
  descripcion: {
    type: String,
    required: true,
    trim: true,
    min: 2
  },
  creador: {
    type: String,
    required: true
  },
  temas: [{
    type : mongoose.Schema.Types.ObjectId,ref:'tema'
  }],
}, {timestamps: true})

module.exports = mongoose.model('foro', foroSchema)