const mongoose = require('mongoose')

const groupSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
    min: 2,
    max: 40,
    unique: true
  },
  descripcion: {
    type: String,
    required: true,
    trim: true,
    min: 2
  },
  administrador: {
    type: String,
    required: true
  },
  usuarios: [{
    type : String
  }],
  foros: [{
    type : mongoose.Schema.Types.ObjectId,ref:'foro'
  }],
}, {timestamps: true})

module.exports = mongoose.model('grupo', groupSchema)