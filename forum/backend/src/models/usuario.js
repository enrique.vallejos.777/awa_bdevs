const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  nombres: {
    type: String,
    required: true,
    trim: true,
    min: 2,
    max: 40
  },
  apellidos: {
    type: String,
    required: true,
    trim: true,
    min: 2,
    max: 40
  },
  correo: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    lowercase: true
  },
  contrasena: {
    type: String,
    required: true
  },
  sexo: {
    type: String,
    required: true,
    enum: ['H', 'M', 'NE']
  },
  fechaNacimiento: {
    type: Date,
    required: true
  },
  fotoPerfil: {
    type: String
  }
}, {timestamps: true})

module.exports = mongoose.model('usuario', userSchema)