const mongoose = require('mongoose')

const temaSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
    min: 2,
    max: 40
  },
  contenido: {
    type: String,
    required: true,
    trim: true,
    min: 2
  },
  creador: {
    type: String,
    required: true
  },
  me_gusta: [{
    type: String
  }],
  no_me_gusta: [{
    type: String
  }],
  me_entristece: [{
    type: String
  }],
  es_grandioso: [{
    type: String
  }],
  posts: [{
    type : mongoose.Schema.Types.ObjectId,ref:'post'
  }]
}, {timestamps: true})

module.exports = mongoose.model('tema', temaSchema)