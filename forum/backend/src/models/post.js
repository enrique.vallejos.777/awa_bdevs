const mongoose = require('mongoose')

const postSchema = new mongoose.Schema({
  contenido: {
    type: String,
    required: true,
    trim: true,
    min: 2
  },
  usuario: {
    type: String,
    required: true
  }
}, {timestamps: true})

module.exports = mongoose.model('post', postSchema)