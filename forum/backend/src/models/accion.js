const mongoose = require('mongoose')

const actionSchema = new mongoose.Schema({
  ip: {
    type: String,
    required: true,
    trim: true,
    min: 7
  },
  pais: {
    type: String,
    required: true,
    trim: true,
    min: 2,
    max: 5
  },
  ciudad: {
    type: String,
    required: true,
    trim: true,
    min: 2,
    max: 40
  },
  latitud: {
    type: Number,
    required: true,
  },
  longitud: {
    type: Number,
    required: true
  },
  metodo: {
    type: String,
    required: true,
    enum: ['POST', 'GET', 'PUT']
  },
  ruta: {
    type: String,
    required: true,
    min: 5
  },
  codigo_respuesta: {
    type: Number,
    required: true
  },
  usuario: {
    type: String
  }
}, {timestamps: true})

module.exports = mongoose.model('accion', actionSchema)