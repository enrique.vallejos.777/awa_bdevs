const express = require('express')
const cors = require('cors')
const env = require('dotenv')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const middleware = require('./middleware')

//routes
const userRoutes = require('./routes/usuario')
const groupRoutes = require('./routes/grupo')
const foroRoutes = require('./routes/foro')

//Env configuration
env.config()

//Mongodb conexion
//mongodb+srv://batistools:<password>@cluster0.sf6st.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
mongoose.connect(
  `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PSW}@cluster0.tazvm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  }
).then(() => {
  console.log('Database connected')
})

app.use(cors())
app.use(express.urlencoded({ extended: true }))
app.use(express.json());

app.use(middleware.audit)
app.use('/api', userRoutes)

app.use(middleware.verify)
app.use('/api', groupRoutes)
app.use('/api', foroRoutes)

app.listen(process.env.PORT, ()=> {
  console.log(`Server is running on port ${process.env.PORT}`)
})