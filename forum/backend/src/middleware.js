const jwt = require('jsonwebtoken')
const tokenSecret = "X7CGdBHa3yO9FfBh6RpRgp3uWkFG2nGF"
const ip2loc = require("ip2location-nodejs")
const Action = require('./models/accion')

function localizacion(ip, pais, ciudad, latitud, longitud, metodo, ruta, codigo_respuesta, usuario) {
  const _action = new Action({
    ip,
    pais,
    ciudad,
    latitud,
    longitud,
    metodo,
    ruta,
    codigo_respuesta,
    usuario
  })
  _action.save()
  return _action
}

exports.audit = (req, res, next) => {
  res.on('finish', () => {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var pais     = "LO"
    var ciudad   = "Local"
    var latitud  = 0
    var longitud = 0

    if (ip !== "::1" && ip !== "127.0.0.1" && ip !== "::ffff:127.0.0.1" && ip !== "::1:127.0.0.1") {
      ip2loc.IP2Location_init("ips/IPs.BIN")
      allInfo = ip2loc.IP2Location_get_all(ip)
      pais     = allInfo.country_short
      ciudad   = allInfo.city
      latitud  = allInfo.latitude
      longitud = allInfo.longitude
    }

    const metodo = req.method
    const ruta   = req.originalUrl
    
    if (req.usuario) {
      localizacion(ip, pais, ciudad, latitud, longitud, metodo, ruta, res.statusCode, req.usuario.correo)
    } else {
      localizacion(ip, pais, ciudad, latitud, longitud, metodo, ruta, res.statusCode, null)
    }
  });
  
  next()
}

exports.verify = (req, res, next) => {
  const token = req.headers.authorization
  if (!token) {
    res.status(403).json({ error: "Please, provide a token" })
  } else {
    jwt.verify(token, tokenSecret, (error, value) => {
      if (error == 'TokenExpiredError: jwt expired') {
        res.status(440).json({ error: 'Failed to authenticate token: ' + error })
      } else if (error) {
        res.status(403).json({ error: 'Failed to authenticate token: ' + error })
      } else {
        req.usuario = value.data
        next()
      }
    })
  }
}
