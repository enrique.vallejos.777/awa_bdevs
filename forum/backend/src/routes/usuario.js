const express = require('express')
const router  = express.Router()
const bcrypt  = require('bcryptjs');
const jwt     = require('jsonwebtoken')
const User    = require('../models/usuario')
const {check, validationResult} = require('express-validator');

const saltRounds  = 10;
const tokenSecret = "X7CGdBHa3yO9FfBh6RpRgp3uWkFG2nGF"

function sexoValido(sexo) {
  res = false
  
  if (sexo == "H" || sexo == "M" || sexo == "NE") {
    res = true
  }
  
  return res
}

function correoRegistrado(correo) {
  return new Promise((resolve, reject) => {
    return User.findOne({ correo: correo }).exec((error, usuario) => {
      if (usuario) {
        reject('Email already in use');
      }
      resolve();
    })
  })
}

function generateToken(usuario) {
  return jwt.sign({ data: usuario }, tokenSecret, { expiresIn: '24h' })
}

router.post(
  '/ingresar', 
  [
    check('correo').isEmail(),
    check('contrasena').isLength({ min: 2, max: 128 }),
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    
    const {
      correo,
      contrasena
    } = req.body

    req.usuario = {}
    req.usuario.correo = correo
    
    User.findOne({correo: correo}).exec((error, usuario) => {
      if (!usuario) {
        return res.status(404).json({ message: 'No user with that email found' })
      } else {
        bcrypt.compare(contrasena, usuario.contrasena, (error, match) => {
          if (error) {
            return res.status(500).json({ error: error })
          } else if (match) {
            return res.status(200).json({ token: generateToken(usuario) })
          } else {
            return res.status(403).json({ message: 'Invalid password' })
          }
        })
      }
    })
  }
)

router.post(
  '/registrar', 
  [
    check('nombres').isLength({ min: 2, max: 40 }),
    check('apellidos').isLength({ min: 2, max: 40 }),
    check('correo').isEmail().custom(correo => correoRegistrado(correo)),
    check('contrasena').isLength({ min: 2, max: 128 }),
    check('sexo').custom(sexo => { return sexoValido(sexo) }),
    check('fechaNacimiento').isDate()
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    
    var {
      nombres,
      apellidos,
      correo,
      contrasena,
      sexo,
      fechaNacimiento
    } = req.body
    
    bcrypt.hash(contrasena, saltRounds, (error, hashedContrasena) => {
      if (error) {
        return res.status(400).json({ message: 'Error while hashing pass: '+error })
      } else {
        contrasena = hashedContrasena
        const _user = new User({
          nombres,
          apellidos,
          correo,
          contrasena,
          sexo,
          fechaNacimiento
        })

        _user.save((error, data) => {
          if (error) {
            return res.status(400).json({ message: 'Error while saving data: '+error })
          } else {
            return res.status(201).json({ message: "Registered successfully" })
          }
        })
      }
    })
  }
)

module.exports = router