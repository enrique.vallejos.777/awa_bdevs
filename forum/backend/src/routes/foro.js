const express = require('express')
const router  = express.Router()
const Group   = require('../models/grupo')
const User    = require('../models/usuario')
const Forum   = require('../models/foro')
const Topic   = require('../models/tema')
const Post    = require('../models/post')
const {check, validationResult} = require('express-validator');

function grupoRegistrado(nombre) {
  return new Promise((resolve, reject) => {
    return Group.findOne({ nombre: nombre }).exec((error, grupo) => {
      if (grupo) {
        reject('Group name already in use');
      }
      resolve();
    })
  })
}

function usuarioRegistrado(correo) {
  return new Promise((resolve, reject) => {
    return User.findOne({ correo: correo }).exec((error, usuario) => {
      if (!usuario) {
        reject('User not found');
      }
      resolve();
    })
  })
}

function miembroGrupo(usuario, grupo) {
  return new Promise((resolve, reject) => {
    return Group.findOne({ nombre: grupo, usuarios: usuario })
      .populate({path: 'foros', 
                   populate: {path: 'temas',
                         populate: { path: 'posts' }}})
      .exec((error, grupo) => {
        if (!grupo || error) {
          reject('Group not found or not authorized');
        } else {
          resolve(grupo);
        }
      })
  })
}

function nombreRegistrado(elementos, nombre) {
  let found = null
  
  for(let i = 0; i < elementos.length && !found; i++ ) {
    if(elementos[i].nombre === nombre) {
       found = elementos[i];
    }
  }
  
  return found
}

router.post(
  '/grupo/:grupo/foro',
  [
    check('nombre').isLength({ min: 2, max: 40 }),
    check('descripcion').isLength({ min: 2 }),
    check('grupo').isLength({ min: 2, max: 40 })
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    
    const creador = req.usuario.correo
    const grupo = req.params.grupo
    const {
      nombre,
      descripcion
    } = req.body
    
    miembroGrupo(creador, grupo)
      .then((grupo) => {
        const foros = grupo.foros
        if (nombreRegistrado(foros, nombre)) {
          return res.status(409).json({ message: "There is a forum with the same name in this group" })
        } else {
          const _foro = new Forum ({
            nombre,
            descripcion,
            creador
          })
          
          _foro.save((error, foro) => {
            if (error) {
              return res.status(400).json({ message: 'Error while saving forum data: '+error })
            } else {
              grupo.foros.push(foro)
              grupo.save((error, grupo) => {
                if (error) {
                  return res.status(400).json({ message: 'Error while saving group data: '+error })
                } else {
                  return res.status(201).json({ foro: foro })
                }
              })
            }
          })
        }
      })
      .catch((error) => {
        return res.status(401).json({ message: error })
      })
  }
)

router.post(
  '/grupo/:grupo/foro/:foro',
  [
    check('nombre').isLength({ min: 2, max: 40 }),
    check('contenido').isLength({ min: 2 }),
    check('grupo').isLength({ min: 2, max: 40 }),
    check('foro').isLength({ min: 2, max: 40 })
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    
    const creador = req.usuario.correo
    const grupo = req.params.grupo
    const foro  = req.params.foro
    const {
      nombre,
      contenido
    } = req.body
    
    miembroGrupo(creador, grupo)
      .then((grupo) => {
        const _foro = nombreRegistrado(grupo.foros, foro)
        if (_foro) {
          if(nombreRegistrado(_foro.temas, nombre)) {
            return res.status(409).json({ message: "There is a topic with the same name in this forum" })
          } else {
            const _tema = new Topic ({
              nombre,
              contenido,
              creador
            })

            _tema.save((error, tema) => {
              if (error) {
                return res.status(400).json({ message: 'Error while saving topic data: '+error })
              } else {
                _foro.temas.push(tema)
                _foro.save((error, grupo) => {
                  if (error) {
                    return res.status(400).json({ message: 'Error while saving forum data: '+error })
                  } else {
                    return res.status(201).json({ tema: tema })
                  }
                })
              }
            })
          }
        } else {
          return res.status(409).json({ message: "There is no forum with this name in the group" })
        }
      })
      .catch((error) => {
        return res.status(401).json({ message: error })
      })
  }
)

router.post(
  '/grupo/:grupo/foro/:foro/tema/:tema',
  [
    check('contenido').isLength({ min: 2 }),
    check('grupo').isLength({ min: 2, max: 40 }),
    check('foro').isLength({ min: 2, max: 40 }),
    check('tema').isLength({ min: 2, max: 40 })
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    
    const usuario = req.usuario.correo
    const grupo = req.params.grupo
    const foro  = req.params.foro
    const tema  = req.params.tema
    const {
      contenido
    } = req.body
    
    miembroGrupo(usuario, grupo)
      .then((grupo) => {
        _foro = nombreRegistrado(grupo.foros, foro)
        if (_foro) {
          _tema = nombreRegistrado(_foro.temas, tema)
          if(_tema) {
            const _post = new Post ({
              contenido,
              usuario
            })
            
            _post.save((error, post) => {
              if (error) {
                return res.status(400).json({ message: 'Error while saving post data: '+error })
              } else {
                _tema.posts.push(post)
                _tema.save((error, tema) => {
                  if (error) {
                    return res.status(400).json({ message: 'Error while saving topic data: '+error })
                  } else {
                    return res.status(201).json({ post: post })
                  }
                })
              }
            })
          } else {
            return res.status(409).json({ message: "There is no topic with the name in the forum" })
          }
        } else {
          return res.status(409).json({ message: "There is no forum with this name in the group" })
        }
      })
      .catch((error) => {
        return res.status(401).json({ message: error })
      })
  }
)

router.get(
  '/foros/:grupo',
  (req, res) => {
    const usuario = req.usuario.correo
    const nombreGrupo = req.params.grupo
        
    miembroGrupo(usuario, nombreGrupo)
      .then((grupo) => {
        const foros = grupo.foros
        return res.status(200).json({ foros: grupo.foros })
      })
      .catch((error) => {
        return res.status(401).json({ message: error })
      })
  }
)

module.exports = router