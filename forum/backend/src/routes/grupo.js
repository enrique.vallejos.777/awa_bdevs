const express = require('express')
const router  = express.Router()
const Group   = require('../models/grupo')
const User    = require('../models/usuario')
const {check, validationResult} = require('express-validator');

function grupoRegistrado(nombre) {
  return new Promise((resolve, reject) => {
    return Group.findOne({ nombre: nombre }).exec((error, grupo) => {
      if (grupo) {
        reject('Group name already in use');
      }
      resolve();
    })
  })
}

function usuarioRegistrado(correo) {
  return new Promise((resolve, reject) => {
    return User.findOne({ correo: correo }).exec((error, usuario) => {
      if (!usuario) {
        reject('User not found');
      }
      resolve();
    })
  })
}

router.post(
  '/grupo',
  [
    check('nombre').isLength({ min: 2, max: 40 }).custom(nombre => grupoRegistrado(nombre)),
    check('descripcion').isLength({ min: 2 })
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    
    const {
      nombre,
      descripcion
    } = req.body
    
    const administrador = req.usuario.correo
    const usuarios = [administrador]
    
    const _group = new Group({
      nombre,
      descripcion,
      administrador,
      usuarios
    })
    
    _group.save((error, grupo) => {
      if (error) {
        return res.status(400).json({ message: 'Error while saving data: '+error })
      } else {
        return res.status(201).json({ group: grupo })
      }
    })
  }
)

router.put('/grupo', (req, res) => {
    
  }
)

router.post(
  '/grupo/miembro',
  [
    check('miembro').isEmail().custom(miembro => usuarioRegistrado(miembro)),
    check('grupo').isLength({ min: 2, max: 40 })
  ],
  (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    
    const usuario = req.usuario.correo
    
    const {
      miembro,
      grupo
    } = req.body
    
    Group.findOne({ administrador: usuario, nombre: grupo }).exec((error, grupo) => {
      if (!grupo || error) {
        return res.status(401).json({ message: 'Not Authorized' })
      } else {
        if (!grupo.usuarios.includes(miembro)) {
          grupo.usuarios.push(miembro)
          
          grupo.save((error, grupo) => {
            if (error) {
              return res.status(400).json({ message: 'Error while saving data: '+error })
            } else {
              return res.status(200).json({ group: grupo })
            }
          })
        } else {
          return res.status(409).json({ message: 'User is already a member' })
        }
      }
    })
  }
)

router.delete('/grupo', (req, res) => {
    
  }
)

router.get(
  '/grupo/:nombre',
  (req, res) => {
    const usuario = req.usuario.correo
    const nombreGrupo = req.params.nombre
    
    Group.findOne({ usuarios: usuario, nombre: nombreGrupo }).populate('foros').exec((error, grupo) => {
      if (!grupo || error) {
        return res.status(401).json({ message: 'Group not found or not authorized'})
      } else {
        return res.status(200).json({ group: grupo })
      }
    })
  }
)

router.get(
  '/grupos',
  (req, res) => {
    const usuario = req.usuario.correo
    
    Group.find({ usuarios: usuario }).exec((error, grupos) => {
      return res.status(200).json({ groups: grupos })
    })
  }
)

module.exports = router