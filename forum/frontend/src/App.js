import React from 'react';
import { useState } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { Container, Button } from 'react-bootstrap';

import Login from './components/auth/Login';
import Logout from './components/auth/Logout';
import ModificarPerfil from './components/auth/ModificarPerfil';
import NuevaCuenta from './components/auth/NuevaCuenta';

import Grupos from './components/groups/Grupos';
import GrupoSeleccionado from './components/groups/GrupoSeleccionado';
import NuevoGrupo from './components/groups/NuevoGrupo';

function App() {
  const correo = localStorage.getItem('correo');
  
  const infoUsuario = [];
  if (correo != null) {
    infoUsuario.push(<div key="info_usuario">
                        <div className="mr-2">{correo}</div>
                        <div className="mr-2">
                          <Link to="/cerrar_sesion">
                            <Button variant="danger" size="sm">Cerrar Sesión</Button>
                          </Link>
                        </div>
                     </div>);
  }
  
  return (
    <Router>
      <div className="header p-3 mb-2 bg-dark text-white">
        <Link to={'/'} >
          <h1 className="float-left text-white">FORUM</h1>
        </Link>
        <div className="float-right">
          {infoUsuario} 
        </div>
        <div className="clearfix"></div>
      </div>
      <Container fluid>
        <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/cerrar_sesion" component={Logout} />
            <Route exact path="/modificar_perfil" component={ModificarPerfil}/>
            <Route exact path="/nueva_cuenta" component={NuevaCuenta}/>
                     
            <Route exact path="/grupos" component={Grupos}/>
            <Route exact path="/grupos/nuevo" component={NuevoGrupo}/>
            <Route path="/grupos/seleccion/:nombreGrupo" component={GrupoSeleccionado}/>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
