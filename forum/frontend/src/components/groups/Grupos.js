import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

import axiosCliente from '../../config/axiosCliente';
import { Button, Table } from 'react-bootstrap';
import { Trash, BoxArrowRight, Pencil } from 'react-bootstrap-icons';

const Grupos = (props) => {
  const [tablaGrupos, setTablaGrupos] = useState([])
    
  const getGrupos = () => {
    const correo = localStorage.getItem('correo');
    
    const accionesTD = (grupo) => {
      const accionesView = [];

      if (correo === grupo.administrador) {
        accionesView.push(<td key="acciones_{grupo}">
                      <Button variant="warning" className="mr-1"><Pencil /> Editar</Button>
                      <Button variant="danger" className="mr-1"><Trash /> Borrar</Button>
                     </td>)
      }
      else {
        accionesView.push(<td key="acciones_{grupo}">
                        <Button variant="info"><BoxArrowRight /> Salir</Button>
                      </td>)
      }

      return accionesView;
    }

    const grupoTR = (grupo, number) => {
      const grupoView = [];

      const fecha  = new Date(grupo.createdAt).toLocaleString();
      const nombre = grupo.nombre;
      const ruta   = "/grupos/seleccion/"+nombre;

      grupoView.push(<tr key={ nombre }>
                      <th>
                        <Link to={ ruta }>{ nombre }</Link>
                      </th>
                      <td>{ fecha }</td>
                      { accionesTD(grupo) }
                     </tr>);

      return grupoView;
    }
    
    axiosCliente
      .get("/grupos")
      .then((response) => {
        const auxGrupos = response.data.groups;
        const auxTablaGrupos = [];
        
        for(let i = 0; i < auxGrupos.length; i++) {
          auxTablaGrupos.push(grupoTR(auxGrupos[i]));
        }
      
        setTablaGrupos(auxTablaGrupos);
      })
      .catch(error => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
      });
  }
  
  useEffect(getGrupos, []);
  
  return ( 
    <div className="p-3 mx-5 text-dark">
      <div className="pb-4 text-center">
        <h2 className="text-center">MIS GRUPOS</h2>
        <Link to="/grupos/nuevo" className="btn btn-dark">
          Nuevo grupo
        </Link>
      </div>

      <Table responsive hover>
        <thead className="thead-dark">
          <tr>
            <th scope="col">Nombre del grupo</th>
            <th scope="col">Fecha de creación</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          { tablaGrupos }
        </tbody>
      </Table>
    </div>
  );
}
 
export default Grupos;