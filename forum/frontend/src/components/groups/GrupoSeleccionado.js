import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

import axiosCliente from '../../config/axiosCliente';
import { Button, Modal, Table } from 'react-bootstrap';
import { Trash } from 'react-bootstrap-icons';

const GrupoSeleccionado = (props) => {
  const { nombreGrupo } = props.match.params;
  const [grupo, setGrupo] = useState({});
  
  const [tablaMiembros, setTablaMiembros] = useState([]);
  const [tablaForos, setTablaForos] = useState([]);
  
  const [showMiembro, setshowMiembro] = useState(false);
  const handleCloseMiembro = () => setshowMiembro(false);
  const handleShowMiembro = () => setshowMiembro(true);
  
  const [miembro, setMiembro] = useState("");
  const [foro, setForo] = useState({});
  
  const [showForo, setshowForo] = useState(false);
  const handleCloseForo = () => setshowForo(false);
  const handleShowForo = () => setshowForo(true);
  
  const getGrupo = () => {
    const correo = localStorage.getItem('correo');
    
    const tablaMiembrosView = (miembros, administrador) => {      
      const auxTablaMiembros = [];
      const isAdmin = (administrador === correo);
      for(let i = 0; i < miembros.length; i++) {
        const miembro = miembros[i];
        const auxRowMiembro = [];
        auxRowMiembro.push(<th scope="row">{miembro}</th>);
        if (isAdmin) {
          auxRowMiembro.push(<th><Button variant="outline-danger" size="sm">X</Button></th>);
        }
        auxTablaMiembros.push(<tr key={miembro}>{auxRowMiembro}</tr>)
      }
      
      setTablaMiembros(auxTablaMiembros);
    }
      
    const tablaForosView = (foros) => {
      const auxTablaForos = [];
      for(let i = 0; i < foros.length; i++) {
        const foro = foros[i].nombre;
        auxTablaForos.push(<tr key={foro}><th scope="row">{foro}</th></tr>)
      }
      
      setTablaForos(auxTablaForos);
    }
    
    axiosCliente
      .get("/grupo/"+nombreGrupo)
      .then((response) => {
        const auxGrupo = response.data.group;
        setGrupo(auxGrupo);
        tablaMiembrosView(auxGrupo.usuarios, auxGrupo.administrador);
        tablaForosView(auxGrupo.foros);
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
        props.history.push('/grupos');
      });
  }

  const handleChangeMiembro = (e) => {
    setMiembro(e.target.value);
  } 

  const handleSubmitMiembro = (e) => {  
    e.preventDefault();
    agregarMiembro();
  }
  
  const agregarMiembro = () => {
    const data = { miembro: miembro, grupo: nombreGrupo }
    
    axiosCliente.post('/grupo/miembro', data)
      .then(res => {
        handleCloseMiembro();
        setMiembro("");
      })
      .catch(error => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
      })
  }
  
  useEffect(getGrupo, []);
    
  return (
    <div className="p-3 mx-5 text-dark">
      <div className="mb-3 pb-3 text-center">
        <h2>{ grupo.nombre }</h2>
        <p>{ grupo.descripcion }</p>
        <Link to={'/grupos'}>(Ir a mis grupos)</Link>
      </div>
      <div className="row">
        <div className="col-sm-2 pb-4 text-center">
          <Table responsive hover>
            <thead className="thead-dark">
              <tr>
                <th scope="col">Miembros</th>
                <th scope="col"><Button variant="secondary" size="sm" onClick={handleShowMiembro}>+</Button></th>
              </tr>
            </thead>
            <tbody>
                { tablaMiembros }
            </tbody>
          </Table>
        </div>
  
        <Modal show={showMiembro} onHide={handleCloseMiembro} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title>Añadir Miembro</Modal.Title>
          </Modal.Header>
          <form onSubmit={handleSubmitMiembro}>
            <Modal.Body>
              <div className="form-group">
                <label htmlFor="email">Correo electrónico:</label>
                <input
                  type      = "email"
                  className = "form-control"
                  name      = "correo"
                  placeholder  = "Ingresa el correo del usuario"
                  defaultValue = { miembro }
                  onChange = { handleChangeMiembro }
                  required
                />
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={ handleCloseMiembro }>
                Cerrar
              </Button>
              <Button type="submit">
                Añadir
              </Button>
            </Modal.Footer>
          </form>
        </Modal>
    
        <div className="col-sm-4 pb-4 text-center">
          <Table responsive hover>
            <thead className="thead-dark">
              <tr>
                <th scope="col">Foros</th>
                <th scope="col"><Button variant="secondary" size="sm" onClick={handleShowForo}>+</Button></th>
              </tr>
            </thead>
            <tbody>
                { tablaForos }
            </tbody>
          </Table>
        </div>
  
        <Modal show={ showForo } onHide={ handleCloseForo } animation={false}>
          <Modal.Header closeButton>
            <Modal.Title>Nuevo Foro</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <div className="form-group">
                <label htmlFor="nombre">Nombre:</label>
                <input
                  type      = "text"
                  className = "form-control"
                  name      = "nombre"
                  placeholder  = "Ingresa el nombre del foro"
                  defaultValue = { foro.nombre }
                  onChange = { handleChangeMiembro }
                  required
                />
              </div>
    
              <div className="form-group">
                <label htmlFor="descripcion">Descripción:</label>
                <textarea
                  rows      = "3"
                  className = "form-control"
                  name      = "descripcion"
                  placeholder  ="Ingresa una descripción para el foro"
                  defaultValue ={ foro.descripcion }
                  onChange = { handleChangeMiembro }
                  required
                />
              </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={ handleCloseForo }>
              Cerrar
            </Button>
            <Button variant="primary" onClick={ handleCloseForo }>
              Añadir
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </div>
  );
}
 
export default GrupoSeleccionado;