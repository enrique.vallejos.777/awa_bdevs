import React, { useState } from 'react';
import {
  withRouter,
  Link
} from "react-router-dom";

import axiosCliente from '../../config/axiosCliente';
import { Button } from 'react-bootstrap';

const NuevoGrupo = (props) => {
  const [grupo, setGrupo] = useState({
    nombre: '',
    descripcion: ''
  });

  const handleChange = (e) =>{
    setGrupo({
      ...grupo,
      [e.target.name]:e.target.value
    })
  }    

  const handleSubmit = (e) =>{
    e.preventDefault();
    guardarGrupo();
  };

  const guardarGrupo = () => {
    axiosCliente.post('/grupo', grupo)
      .then(res=> {
        if(res.data.group){
          props.history.push('/grupos');
        }
      })
      .catch(error => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
      })
  }

  return (
    <div className="rounded p-3 mx-5 bg-light text-dark">
      <h2 className="text-center">Nuevo Grupo</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="nombre">Nombre del grupo:</label>
          <input
            type      = "text"
            className = "form-control"
            name      = "nombre"
            placeholder  = "Ingresa el nombre del grupo"
            defaultValue = {grupo.nombre}
            onChange = {handleChange}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="descripcion">Descripción:</label>
          <textarea
            rows      = "3"
            className = "form-control"
            name      = "descripcion"
            placeholder  ="Ingresa una descripción para el grupo"
            defaultValue ={grupo.descripcion}
            onChange = {handleChange}
            required
          />
        </div>
        
        <Button type="submit">Crear Grupo</Button>
      </form>
    
      <p className="text-right"><Link to={'/grupos'}>Atrás</Link></p>
    </div>
  )
}
 
export default withRouter(NuevoGrupo);
