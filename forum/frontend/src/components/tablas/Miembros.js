import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

import axiosCliente from '../../config/axiosCliente';
import { Button, Modal, Table } from 'react-bootstrap';
import { Trash } from 'react-bootstrap-icons';

const TablaMiembros = (props) => {
  const { nombreGrupo } = props.match.params;
  const [grupo, setGrupo] = useState({});
  
  const [tablaMiembros, setTablaMiembros] = useState([]);
  const [tablaForos, setTablaForos] = useState([]);
  
  const [showMiembro, setshowMiembro] = useState(false);
  const handleCloseMiembro = () => setshowMiembro(false);
  const handleShowMiembro = () => setshowMiembro(true);
  
  const [miembro, setMiembro] = useState("");
  const [foro, setForo] = useState({});
  
  const [showForo, setshowForo] = useState(false);
  const handleCloseForo = () => setshowForo(false);
  const handleShowForo = () => setshowForo(true);
  
  const getGrupo = () => {
    const correo = localStorage.getItem('correo');
    
    const tablaMiembrosView = (miembros, administrador) => {      
      const auxTablaMiembros = [];
      const isAdmin = (administrador === correo);
      for(let i = 0; i < miembros.length; i++) {
        const miembro = miembros[i];
        const auxRowMiembro = [];
        auxRowMiembro.push(<th scope="row">{miembro}</th>);
        if (isAdmin) {
          auxRowMiembro.push(<th><Button variant="outline-danger" size="sm">X</Button></th>);
        }
        auxTablaMiembros.push(<tr key={miembro}>{auxRowMiembro}</tr>)
      }
      
      setTablaMiembros(auxTablaMiembros);
    }
      
    const tablaForosView = (foros) => {
      const auxTablaForos = [];
      for(let i = 0; i < foros.length; i++) {
        const foro = foros[i].nombre;
        auxTablaForos.push(<tr key={foro}><th scope="row">{foro}</th></tr>)
      }
      
      setTablaForos(auxTablaForos);
    }
    
    axiosCliente
      .get("/grupo/"+nombreGrupo)
      .then((response) => {
        const auxGrupo = response.data.group;
        setGrupo(auxGrupo);
        tablaMiembrosView(auxGrupo.usuarios, auxGrupo.administrador);
        tablaForosView(auxGrupo.foros);
      })
      .catch(error => {
        console.log(error);
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
        props.history.push('/grupos');
      });
  }

  const handleChangeMiembro = (e) => {
    setMiembro(e.target.value);
  } 

  const handleSubmitMiembro = (e) => {  
    e.preventDefault();
    agregarMiembro();
  }
  
  const agregarMiembro = () => {
    const data = { miembro: miembro, grupo: nombreGrupo }
    
    axiosCliente.post('/grupo/miembro', data)
      .then(res => {
        handleCloseMiembro();
        setMiembro("");
      })
      .catch(error => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
      })
  }
  
  useEffect(getGrupo, []);
    
  return (
    <div>
      <Table responsive hover>
        <thead className="thead-dark">
          <tr>
            <th scope="col">Miembros</th>
            <th scope="col"><Button variant="secondary" size="sm" onClick={ handleShowMiembro }>+</Button></th>
          </tr>
        </thead>
        <tbody>
            { tablaMiembros }
        </tbody>
      </Table>
    
      <Modal show={ showMiembro } onHide={ handleCloseMiembro } animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Añadir Miembro</Modal.Title>
        </Modal.Header>
        <form onSubmit={ handleSubmitMiembro }>
          <Modal.Body>
            <div className="form-group">
              <label htmlFor="email">Correo electrónico:</label>
              <input
                type      = "email"
                className = "form-control"
                name      = "correo"
                placeholder  = "Ingresa el correo del usuario"
                defaultValue = { miembro }
                onChange = { handleChangeMiembro }
                required
              />
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={ handleCloseMiembro }>
              Cerrar
            </Button>
            <Button type="submit">
              Añadir
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
}
 
export default TablaMiembros;