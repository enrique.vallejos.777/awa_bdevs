import React, { useState } from 'react';
import { Link } from "react-router-dom";

import axiosCliente from '../../config/axiosCliente';
import { Button } from 'react-bootstrap';
 
export const Login = (props) => {
  const token = localStorage.getItem('token');
  if (token) {
    props.history.push('/grupos');
  }
  
  const [usuarioLogin, setUsuarioLogin] = useState({
    correo:'',
    contrasena:''
  })

  const handleChange = (e) => {
    setUsuarioLogin({
      ...usuarioLogin,
      [e.target.name]:e.target.value
    })
  } 

  const handleSubmit = (e) => {  
    e.preventDefault();
    verificarUsuario();
  }

  const verificarUsuario = () => {    
    axiosCliente.post('/ingresar', usuarioLogin)
      .then(res => {
        const token  = res.data.token;
        localStorage.setItem('token', token);
        localStorage.setItem('correo', usuarioLogin.correo);
      
        props.history.push('/grupos');
      })
      .catch(error => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
      })
  }

  return (
    <div className="rounded p-3 mx-5 bg-light text-dark">
      <h2 className="text-center">Iniciar Sesión</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">Correo electrónico:</label>
          <input
            type      = "email"
            className = "form-control"
            name      = "correo"
            placeholder  = "Ingresa tu correo"
            defaultValue = {usuarioLogin.correo}
            onChange = {handleChange}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="password">Contraseña</label>
          <input
            type      ="password"
            className = "form-control"
            name      ="contrasena"
            placeholder  ="Ingresa tu contraseña"
            defaultValue ={usuarioLogin.contrasena}
            onChange = {handleChange}
            required
          />
        </div>
        
        <Button type="submit">Iniciar Sesión</Button>
      </form>
    
      <p className="text-right"><Link to={'/nueva_cuenta'}>Crear nueva cuenta</Link></p>
    </div>  
  )
}
 
export default Login;