import React from 'react';
import { Link } from "react-router-dom";
 
export const Logout = (props) => {
  localStorage.removeItem("token");
  localStorage.removeItem("correo");
  props.history.push('/');
  
  return (
    <div className="rounded p-3 mx-5 bg-light text-dark text-center">
      <h2>Sesión Cerrada</h2>
      <p><Link to={'/'}>Iniciar nuevamente</Link></p>
    </div>  
  )
}
 
export default Logout;