import React , {useState} from 'react';
import {
  withRouter,
  Link
} from "react-router-dom";

import axiosCliente from '../../config/axiosCliente';
import { Button } from 'react-bootstrap';

const NuevaCuenta = (props) => {
  const [usuario, setUsuario] = useState({
    nombres: '',
    apellidos: '',
    correo:'',
    contrasena:'',
    sexo:'',
    fechaNacimiento:''
  });

  const handleChange = (e) => {
    setUsuario({
      ...usuario,
      [e.target.name]:e.target.value
    })
  } 

  const handleSubmit = (e) =>{
    e.preventDefault();
    guardarUsuario();};

  const guardarUsuario = () => {
    axiosCliente.post('/registrar', usuario)
      .then(res=> {
        props.history.push('/');
      })
      .catch(error => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
        }
      })
  }

  return (  
    <div className="rounded p-3 mx-5 bg-light text-dark">
      <h2 className="text-center">Crear Cuenta</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="nombre">Nombre:</label>
          <input
            type      = "text"
            className = "form-control"
            name      = "nombres"
            placeholder  = "Ingresa tu nombre"
            defaultValue = {usuario.nombres}
            onChange     = {handleChange}
            required
          />
        </div>
    
        <div className="form-group">
          <label htmlFor="apellidos">Apellidos:</label>
          <input
            type      = "text"
            className = "form-control"
            name      = "apellidos"
            placeholder  = "Ingresa tu apellido"
            defaultValue = {usuario.apellidos}
            onChange     = {handleChange}
            required
          />
        </div>
        
        <div className="form-group">
          <label htmlFor="sexo">Sexo:</label>
          <div className="form-check">
            <input
              type      = "radio"
              className = "form-check-input"
              name      = "sexo"
              value     = "H"
              onChange  = {handleChange}
              id        = "hombre"
              required
            />
            <label className="form-check-label" htmlFor="hombre">Masculino</label>
          </div>

          <div className="form-check">
            <input
              type      = "radio"
              className = "form-check-input"
              name      = "sexo"
              value     = "M"
              onChange  = {handleChange}
              id        = "mujer"
              required
            />
            <label className="form-check-label" htmlFor="mujer">Femenino</label>
          </div>

          <div className="form-check">
            <input
              type      = "radio"
              className = "form-check-input"
              name      = "sexo"
              value     = "NE"
              onChange  = {handleChange}
              id        = "NoEspecificado"
              required
            />
            <label className="form-check-label" htmlFor="NoEspecificado">Prefiero no especificar</label>
          </div>
        </div>
    
        <div className="form-group">
          <label htmlFor="email">Correo electrónico:</label>
          <input
            type      = "email"
            className = "form-control"
            name      = "correo"
            placeholder  = "Ingresa tu correo electrónico"
            defaultValue = {usuario.correo}
            onChange     = {handleChange}
            required
          />
        </div>
    
        <div className="form-group">
          <label htmlFor="nacimiento">Fecha de nacimiento:</label>
          <input
            type      = "date"
            className = "form-control"
            name      = "fechaNacimiento"
            placeholder  = "Ingresa tu fecha de nacimiento"
            defaultValue = {usuario.fechaNacimiento}
            onChange     = {handleChange}
            required
          />
        </div>
    
        <div className="form-group">
          <label htmlFor="password">Contraseña:</label>
          <input
            type      = "password"
            className = "form-control"
            name      = "contrasena"
            placeholder  = "Ingresa tu contraseña"
            defaultValue = {usuario.contrasena}
            onChange     = {handleChange}
            required
          />
        </div>
    
        <div className="form-group">
          <label htmlFor="confirmar">Confirma tu contraseña:</label>
          <input
            type      = "password"
            className = "form-control"
            name      = "confirmar"
            placeholder  = "Repite tu contraseña"
            onChange     = {handleChange}
            required
          />
        </div>
        
        <Button type="submit">Crear Cuenta</Button>
      </form>
    
      <p className="text-right"><Link to={'/'}>Atrás</Link></p>
    </div>  
  )
}
 
export default withRouter( NuevaCuenta);