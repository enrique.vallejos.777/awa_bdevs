import axios from 'axios';

const baseURL = 'http://localhost:4000/api';

const axiosCliente = axios.create({
  baseURL: baseURL
});

axiosCliente.interceptors.request.use(
  config => {
    const token = localStorage.getItem('token');
    config.headers.authorization = token;
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

export default axiosCliente;
